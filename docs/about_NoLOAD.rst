About NoLOAD
============

NoLOAD is a non-linear optimization tool designed to easily design energy system and components.
NoLOAD is open-source and written in Python with the licence `Apache 2.0`_.

A detailed presentation of NoLOAD will soon be available in scientific articles.

.. _`Apache 2.0`: https://www.apache.org/licenses/LICENSE-2.0.html
