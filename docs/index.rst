.. noload documentation master file, created by
   sphinx-quickstart on Fri May 28 17:14:25 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NoLOAD's documentation!
==================================

Introduction to NoLOAD
----------------------
NoLOAD stands for Non-Linear Optimization using Automatic Differentiation.
It aims to be a light non-linear optimization tool for energy components and
systems.
It is an Open Source project located on GitLab at `NoLOAD Gitlab`_, with
available open benchmark examples at `NoLOAD benchmark Gitlab`_

Contents
--------
.. toctree::
   :maxdepth: 2

   about_NoLOAD

.. toctree::
   :maxdepth: 1

   installation_requirements

.. toctree::
   :maxdepth: 2

   how_to_NoLOAD

.. toctree::
   :maxdepth: 2

   NoLOAD_description

.. toctree::
   :maxdepth: 2

   new_functionnalities

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Acknowledgments
---------------

A special thank is adressed to the contributors that are not main authors.

.. _NoLOAD Gitlab: https://gricad-gitlab.univ-grenoble-alpes.fr/design_optimization/noload
.. _NoLOAD benchmark Gitlab: https://gricad-gitlab.univ-grenoble-alpes.fr/design_optimization/noload_benchmarks_open
