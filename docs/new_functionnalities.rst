What's new in the latest versions
=================================

The new version of NoLOAD v1.2.8 is available!
The release is from 19th of February 2024.

.. toctree::
   :maxdepth: 2

   new_features/v1.2.8


Information about the latest versions
-------------------------------------

.. toctree::
   :maxdepth: 1

   new_features/v1.2.7
   new_features/v1.2.6
   new_features/v1.2.5
   new_features/v1.2.4
   new_features/v1.2.3
   new_features/v1.2.2
   new_features/v1.2.1
   new_features/v1.2.0
   new_features/v1.1.0
   new_features/v1.0.3
