What’s new in version 1.0.3
===========================
Version available since 28th May 2020

Bug fixed
---------
- bug correction of double bounded constraints

New Features
------------
- When no solution is found for an optimization, try initial random guess and re-run the optimisation process.

Pareto
++++++
- New parameter `joinDots` to join the Pareto dots (True) or not (False) in plotPareto

Contributors
------------
Benoit Delinchant
Sacha Hodencq