<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "Bug" label, following this link :

- https://gricad-gitlab.univ-grenoble-alpes.fr/design_optimization/noload/-/issues

and verify the issue you're about to submit isn't a duplicate.
--->

### Summary

(Summarize the bug encountered concisely)

### Steps to reproduce

(How one can reproduce the issue - this is very important)

### Example Project

(If possible, please create an example project here on GitLab.com that exhibits the problematic behaviour, and link to it here in the bug report)

(If you are using an older version of GitLab, this will also determine whether the bug has been fixed in a more recent version)

### What is the current *bug* behavior?

(What actually happens)

### What is the expected *correct* behavior?

(What you should see instead)

### Relevant logs and/or screenshots, python output

(Paste any relevant logs)

### Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

### Note : we will only investigate if the tests are passing

/label ~Bug
