from timeit import timeit
import autograd.numpy as np
from autograd import jacobian
from scipy import optimize

def test(x):
    return np.sin(x)**2

def df(x):
    return np.diag(2*np.sin(x)*np.cos(x))

jac_test = jacobian(test)


def approx_j(x0, func, eps, *args):
    """Numerical jacobian."""
    f0 = func(*((x0, ) + args))
    jac = np.zeros([len(x0), len(f0)])
    dx = np.zeros(len(x0))
    for i in range(len(x0)):
        dx[i] = eps
        jac[i] = (func(*((x0 + dx, ) + args)) - f0) / eps
        dx[i] = 0.0
    return jac.transpose()


# confirm jacobian
assert np.all(np.isclose(
    jac_test(np.zeros(4)),
    approx_j(np.zeros(4), test, 1e-8)
))
assert np.all(np.isclose(
    df(np.zeros(4)),
    approx_j(np.zeros(4), test, 1e-8)
))

# find root with and without jacobian
s1 = optimize.root(
    test, np.ones(4), method='hybr'
)
s2 = optimize.root(
    test, np.ones(4), method='hybr', jac=jac_test
)
s3 = optimize.root(
    test, np.ones(4), method='hybr', jac=df
)

# confirm same solution
assert np.all(np.isclose(s1.x, s2.x))
assert np.all(np.isclose(s1.x, s3.x))
# using jacobian should require fewer function evaluations
assert s2.nfev <= s1.nfev
assert s3.nfev <= s1.nfev

# timeit
cmd = """
optimize.root(
    test, np.ones(4), method='hybr'{}
)
"""
print("find root performances")
print(
    'Without jacobian:',
    timeit(cmd.format(''), globals=globals(), number=1000)
)
print(
    'With autograd jacobian:',
    timeit(cmd.format(', jac=jac_test'), globals=globals(), number=1000)
)
print(
    'With analytical jacobian:',
    timeit(cmd.format(', jac=df'), globals=globals(), number=1000)
)