# SPDX-FileCopyrightText: 2021 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

from numpy.testing import *
from noload.optimization.optimProblem import Spec, OptimProblem

#Fonction sans paramètres
def f_test_1d(x):
    fobj = (x-1)**2
    return locals().items()

def f_test_2d(x, y):
    fobj = (x-1)**2 + (y-1)**2
    return locals().items()

#Fonction avec paramètres
def f_test_2d_p(x, y, a, b):
    fobj = a*(x-1)**2 + b*(y-1)**2
    return locals().items()

#Fonction avec contrainte
def f_test_2d_p_c(x, y, a, b):
    fobj = a*(x-1)**2 + b*(y-1)**2
    ctr1 = (x-1)
    ctr2 = (y-1)
    return locals().items()

class test_simples_exhaustifs(TestCase):
    def test_scalaires(self):
        decimal = 4
        accuracy = 10 ** (-decimal-5)

        # Fonction sans paramètres à 1 variable
        spec = Spec(variables={'x': 0}, bounds={'x':[-5, 5]},
                    objectives={'fobj':[0.,36.]})

        optim = OptimProblem(model=f_test_1d, specifications=spec)
        result = optim.run(ftol=accuracy)

        assert_almost_equal(result.solution(), [1], decimal)
        assert_almost_equal(result.getLastOutputs()['fobj'], 0, decimal)
        print('\n')

        # Fonction sans paramètres à 2 variables
        spec = Spec(variables={'x':0, 'y':0}, bounds={'x':[-5, 5], 'y':[-5, 5]},
                    objectives={'fobj':[0.,72.]})

        optim = OptimProblem(model=f_test_2d, specifications=spec)
        result = optim.run(ftol=accuracy)

        assert_almost_equal(result.solution(), [1, 1], decimal)
        assert_almost_equal(result.getLastOutputs()['fobj'], 0, decimal)
        print('\n')

        # Fonction avec paramètres
        param= {'a': 1.1, 'b':2.2}
        spec = Spec(variables={'x':0, 'y':0}, bounds={'x':[-5, 5], 'y':[-5, 5]},
                    objectives={'fobj':[0.,180.]})

        optim = OptimProblem(model=f_test_2d_p, specifications=spec,
                             parameters=param)
        result = optim.run(ftol=accuracy)

        assert_almost_equal(result.solution(), [1, 1], decimal)
        assert_almost_equal(result.getLastOutputs()['fobj'], 0, decimal)
        print('\n')

        # Fonction avec contrainte d'égalité
        param= {'a': 1.1, 'b':2.2}
        spec = Spec(variables={'x':0.,'y':0.}, bounds={'x':[-5, 5],'y':[-5, 5]},
                    objectives={'fobj':[0.,180.]}, eq_cstr={'ctr1': 1})

        optim = OptimProblem(model=f_test_2d_p_c, specifications=spec,
                             parameters=param)
        result = optim.run(ftol=accuracy)

        assert_almost_equal(result.solution(), [2, 1], decimal)
        assert_almost_equal(result.getLastOutputs()['fobj'], 1.1, decimal)
        assert_almost_equal(result.getLastOutputs()['ctr1'], 1, decimal)

        print('\n')
        # Fonction avec contrainte de supériorité
        param= {'a': 1.1, 'b':2.2}
        spec = Spec(variables={'x':0, 'y':0}, bounds={'x':[-5, 5], 'y':[-5, 5]},
                    objectives={'fobj':[0.,180.]}, ineq_cstr={'ctr1':[1, None]})

        optim = OptimProblem(model=f_test_2d_p_c, specifications=spec,
                             parameters=param)
        result = optim.run(ftol=accuracy)
        print(result.solution())
        print(result.getLastOutputs()['fobj'])
        print(result.getLastOutputs()['ctr1'])

        assert_almost_equal(result.solution(), [2, 1], decimal)
        assert_almost_equal(result.getLastOutputs()['fobj'], 1.1, decimal)
        assert_almost_equal(result.getLastOutputs()['ctr1'], 1, decimal)

        print('\n')
        # Fonction avec contrainte d'infériorité
        param= {'a': 1.1, 'b':2.2}
        spec = Spec(variables={'x':0, 'y':0}, bounds={'x':[-5, 5], 'y':[-5, 5]},
                    objectives={'fobj':[0.,180.]}, ineq_cstr={'ctr1':[None,-1]})

        optim = OptimProblem(model=f_test_2d_p_c, specifications=spec,
                             parameters=param)
        result = optim.run(ftol=accuracy)

        print(result.solution())
        print(result.getLastOutputs()['fobj'])
        print(result.getLastOutputs()['ctr1'])

        assert_almost_equal(result.solution(), [0, 1], decimal)
        assert_almost_equal(result.getLastOutputs()['fobj'], 1.1, decimal)
        assert_almost_equal(result.getLastOutputs()['ctr1'], -1, decimal)

        print('\n')
        # Fonction avec double contrainte d'inégalité
        param= {'a': 1.1, 'b':2.2}
        spec = Spec(variables={'x':0,'y':0}, bounds={'x':[-5, 5],'y':[-5, 5]},
                    objectives={'fobj':[0.,180.]},ineq_cstr={'ctr1':[-1.5,-1]})

        optim = OptimProblem(model=f_test_2d_p_c, specifications=spec,
                             parameters=param)
        result = optim.run(ftol=accuracy)

        print(result.solution())
        print(result.getLastOutputs()['fobj'])
        print(result.getLastOutputs()['ctr1'])

        assert_almost_equal(result.solution(), [0, 1], decimal)
        assert_almost_equal(result.getLastOutputs()['fobj'], 1.1, decimal)
        assert_almost_equal(result.getLastOutputs()['ctr1'], -1, decimal)

