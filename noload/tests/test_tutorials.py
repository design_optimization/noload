# SPDX-FileCopyrightText: 2021 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

from numpy.testing import *
from noload.optimization.optimProblem import Spec, OptimProblem
import autograd.numpy as np
import math


class test_tutorials(TestCase):
    def test_Tuto1_UnconstrainedMonoObjective(self):
        def ackley(x, y):
            fobj = -20 * np.exp(-0.2 * np.sqrt(0.5 * (np.square(x) +
                np.square(y)))) - np.exp(0.5 * (np.cos(2 * math.pi * x) +
                np.cos(2 * math.pi * y))) + math.exp(1) + 20
            return locals().items()
        decimal = 4
        accuracy = 10 ** (-decimal-1)
        spec = Spec(variables={'x':2, 'y':2}, bounds={'x':[-5, 5], 'y':[-5, 5]},
                    objectives={'fobj':[0.,15.]})
        optim = OptimProblem(model=ackley, specifications=spec)
        result = optim.run(ftol=accuracy)
        assert_almost_equal(result.solution(), [0, 0], decimal)
        assert_almost_equal(result.getLastOutputs()['fobj'], 0, decimal)

    def test_Tuto2_ConstrainedMonoObjective(self):
        def simionescu(x, y, rr, rs, n):
            fobj = 0.1 * x * y
            ctr = x * x + y * y - np.power(rr + rs * np.cos(n*np.arctan(x/y)),2)
            return locals().items()
        decimal = 4
        accuracy = 10 ** (-decimal-1)
        # Optimize
        from noload.optimization.optimProblem import Spec, OptimProblem
        spec = Spec(variables={'x':0, 'y':1}, bounds={'x':[-1.25, 1.25],
            'y':[-1.25, 1.25]}, objectives={'fobj':[0.,0.15]},
                    ineq_cstr={'ctr': [None, 0]}  # inequality constraints
                   )
        optim = OptimProblem(model=simionescu, specifications=spec,
                             parameters={'rr': 1, 'rs': 0.2, 'n': 8})
        result = optim.run(ftol=accuracy)

        assert_almost_equal(result.solution(), [-0.84852813,0.84852813],decimal)
        assert_almost_equal(result.getLastOutputs()['fobj'], -0.072, decimal)
        assert_array_less(result.getLastOutputs()['ctr'], [accuracy], decimal)


    def test_Tuto3_ConstrainedMuliObjective(self):
        def BinhAndKorn(x, y):
            f1 = 4 * x ** 2 + 4 * y ** 2
            f2 = (x - 5) ** 2 + (y - 5) ** 2
            g1 = (x - 5) ** 2 + y
            g2 = (x - 8) ** 2 + (y + 3) ** 2
            return locals().items()

        decimal = 4
        accuracy = 10 ** (-decimal-1)

        spec = Spec(variables={'x':0, 'y':0}, bounds={'x':[0, 5], 'y':[0, 3]},
                    objectives={'f1':[0.,140.],'f2':[0.,50.]},
                   ineq_cstr={'g1':[None, 25],'g2':[20, None]}, #inequality
                    # constraints
                   )
        optim = OptimProblem(model=BinhAndKorn, specifications=spec)
        result = optim.run(ftol=accuracy,nbParetoPts=5)
        result.plotPareto(['BinhAndKorn'],'Pareto Front',nb_annotation=5)
        inp,out=result.getIteration(4)
        assert_almost_equal(list(inp.values()),[3.6968455,3.0],3)

        f1i = result.resultsHandler.oNames.index('f1')
        f2i = result.resultsHandler.oNames.index('f2')
        g1i = result.resultsHandler.oNames.index('g1')
        g2i = result.resultsHandler.oNames.index('g2')

        sols = result.resultsHandler.solutions
        o_x = []
        o_y = []
        o_f1 = []
        o_f2 = []
        o_g1 = []
        o_g2 = []

        for sol in sols:
            o_x.append(sol.iData[0])
            o_y.append(sol.iData[1])
            o_f1.append(sol.oData[f1i])
            o_f2.append(sol.oData[f2i])
            o_g1.append(sol.oData[g1i])
            o_g2.append(sol.oData[g2i])

        x_ref=[0.0,5.0,2.3804761441617255,3.696845502192493,1.008384747383488]
        y_ref=[0.0,2.9999999999999787,2.380476141707381,3.0,1.0088955017609549]

        # WARNING: ce test ne fonctionnera plus si on change un peu la methode
        # calcul des points du Pareto...
        # a faire: définir une interpolation des fonctions et verifier si les points
        # du pareto sont proche de cette interpolation
        x=[0,1.0086,2.38047,3.69684,5.]
        y=[0,1.0087,2.38047,3,3.]
        f1=[0,8.13884,45.33333,90.66666,136]
        f2=[50,31.86190,13.72381,5.69821,4]
        g1=[25,16.9397,9.24238,4.698211644,3]
        g2=[73,64.9483,60.52857,54.51713,45]

        assert_almost_equal(o_x, x, decimal)
        assert_almost_equal(o_y, y, decimal)
        assert_almost_equal(o_f1, f1, decimal)
        assert_almost_equal(o_f2, f2, decimal)
        assert_almost_equal(o_g1, g1, decimal)
        assert_almost_equal(o_g2, g2, decimal)



