# SPDX-FileCopyrightText: 2021 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

from numpy.testing import *
from noload.optimization.optimProblem import Spec, OptimProblem

def f_cstr_vect(x, y, a, b):
    fobj = a*(x-1)**2 + b*(y-1)**2
    ctr1 = x-1
    ctr2 = [y-1,x-2*y]
    return locals().items()

def f_mixted_cstr(x, y, z, a, b):
    fobj = a*(x-1)**2 + b*(y-1)**2 + (z-1)**2
    ctr1 = x-1
    ctr2 = [y-1,x-2*z]
    return locals().items()

def f_mixted_var(x, y , a, b):
   fobj = a*(x-1)**2 + b*(y[0]-1)**2 + (y[1]-1)**2
   ctr1 = x-1
   ctr2 = [y[0]-1,x-2*y[1]]
   return locals().items()

class test_contraintes_mixtes(TestCase):
    def test_vectoriels(self):
        decimal = 4
        accuracy = 10 ** (-decimal-5)

        # Fonction avec variables d'entrées mixtes
        param = {'a': 1.1, 'b': 2.2}
        spec = Spec(variables={'x':0, 'y': [0, 0]}, bounds={'x':[-5, 5],
          'y':[[-5,5],[-5,5]]},objectives={'fobj':[0.,180.]},eq_cstr={'ctr1':2})

        optim = OptimProblem(model=f_mixted_var, specifications=spec,
                             parameters=param)
        result = optim.run(ftol=accuracy)

        print(result.solution())
        print(result.getLastOutputs()['fobj'])
        print(result.getLastOutputs()['ctr1'])

        assert_almost_equal(result.getLastOutputs()['fobj'], 4.4, decimal)
        assert_almost_equal(result.getLastOutputs()['ctr1'], [2], decimal)

        # Fonction avec vecteur contrainte d'égalité
        spec = Spec(variables={'x':0, 'y':0} ,bounds={'x':[-5, 5], 'y':[-5, 5]},
                    objectives={'fobj':[0.,180.]},
                   eq_cstr={'ctr2':[-1,2]})

        optim = OptimProblem(model=f_cstr_vect, specifications=spec,
                             parameters=param)
        result = optim.run(ftol=accuracy)

        print(result.solution())
        print(result.getLastOutputs()['fobj'])
        print(result.getLastOutputs()['ctr2'])

        assert_almost_equal(result.solution(), [2, 0], decimal)
        assert_almost_equal(result.getLastOutputs()['fobj'], 3.3, decimal)
        assert_almost_equal(result.getLastOutputs()['ctr2'],[-1,2], decimal)

        print('\n')

        # Fonction avec double contrainte d'égalité
        spec = Spec(variables={'x':0, 'y':0, 'z':0}, bounds={'x':[-5, 5],
            'y':[-5, 5],'z':[-5, 5]}, objectives={'fobj':[0.,180.]},
                    eq_cstr={'ctr1':1,'ctr2':[-1,2]})

        optim = OptimProblem(model=f_mixted_cstr, specifications=spec,
                             parameters=param)
        result = optim.run(ftol=accuracy)

        print(result.solution())
        print(result.getLastOutputs()['fobj'])
        print(result.getLastOutputs()['ctr1'])
        print(result.getLastOutputs()['ctr2'])

        assert_almost_equal(result.solution(), [2, 0, 0], decimal)
        assert_almost_equal(result.getLastOutputs()['fobj'], 4.3, decimal)
        assert_almost_equal(result.getLastOutputs()['ctr1'],1, decimal)
        assert_almost_equal(result.getLastOutputs()['ctr2'],[-1,2], decimal)

        # Fonction avec vecteur contrainte d'inégalité
        spec = Spec(variables={'x':0, 'y':0}, bounds={'x':[-5, 5], 'y':[-5, 5]},
            objectives={'fobj':[0.,180.]}, ineq_cstr={'ctr2': [[-1, 2],[-2,3]]})

        optim = OptimProblem(model=f_cstr_vect, specifications=spec,
                             parameters=param)
        result = optim.run(ftol=accuracy)

        print(result.solution())
        print(result.getLastOutputs()['fobj'])
        print(result.getLastOutputs()['ctr2'])

        assert_almost_equal(result.solution(), [1., 1.], decimal)
        assert_almost_equal(result.getLastOutputs()['fobj'], 0.0,decimal)
        assert_almost_equal(result.getLastOutputs()['ctr2'], [0, -1], decimal)

        print('\n')

        # Fonction avec vecteur contrainte d'inégalité dont l'une uniquement
        # supériorité et l'autre infériorité
        spec = Spec(variables={'x':0., 'y':0.}, bounds={'x':[-5, 5],'y':[-5,5]},
          objectives={'fobj':[0.,180.]},ineq_cstr={'ctr2':[[-1,None],[None,3]]})

        optim = OptimProblem(model=f_cstr_vect, specifications=spec,
                             parameters=param)
        result = optim.run(ftol=accuracy)

        print(result.solution())
        print(result.getLastOutputs()['fobj'])
        print(result.getLastOutputs()['ctr2'])

        assert_almost_equal(result.solution(), [1, 1], decimal)
        assert_almost_equal(result.getLastOutputs()['fobj'], 0.0, decimal)
        assert_almost_equal(result.getLastOutputs()['ctr2'], [0, -1], decimal)

        print('\n')

        # Fonction avec double contrainte d'inégalité
        spec = Spec(variables={'x':0., 'y':0., 'z':0.}, bounds={'x':[-5, 5],
            'y':[-5, 5],'z':[-5, 5]}, objectives={'fobj':[0.,180.]},
                    ineq_cstr={'ctr1':[-1.5, None], 'ctr2':[[-1, 2], [None,3]]})

        optim = OptimProblem(model=f_mixted_cstr, specifications=spec,
                             parameters=param)
        result = optim.run(ftol=accuracy)

        print(result.solution())
        print(result.getLastOutputs()['fobj'])
        print(result.getLastOutputs()['ctr1'])
        print(result.getLastOutputs()['ctr2'])

        assert_almost_equal(result.solution(), [1, 1, 1], decimal)
        assert_almost_equal(result.getLastOutputs()['fobj'], 0.0, decimal)
        assert_almost_equal(result.getLastOutputs()['ctr1'], 0.0, decimal)
        assert_almost_equal(result.getLastOutputs()['ctr2'], [0, -1], decimal)

